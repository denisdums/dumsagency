var gulp = require('gulp');
var sass = require('gulp-sass')(require('node-sass'));
const browserSync = require('browser-sync').create();

// Compilation du SCSS en CSS
function style (){
    return gulp.src('./assets/styles/scss/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('./assets/styles/css/'))
        .pipe(browserSync.stream());
}

function watch(){
    browserSync.init({
        proxy: 'dums-agency.run',
        port: 81
    });
    gulp.watch('./assets/styles/scss/**/*.scss', style);
    gulp.watch('./**/*.php').on('change', browserSync.reload);
    gulp.watch('./assets/styles/css/*.css').on('change', browserSync.reload);
    gulp.watch('./assets/js/**/*.js').on('change', browserSync.reload);
}

exports.style = style;
exports.watch = watch;


