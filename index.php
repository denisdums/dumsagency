<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="icon" type="image/png" href="/assets/images/icon.png"/>
    <link rel="stylesheet" href="/assets/styles/css/app.css">

    <title>DumsAgency | Home</title>
</head>
<body id="start">
<header>
    <div class="da-container">
        <a href="#start" class="da-logo-wrapper">
            <img src="/assets/images/logo.svg" alt="Logo de l'agence web DumsAgency">
        </a>
    </div>
</header>

<input type="checkbox" id="da-contact-target">
<label class="da-contact-trigger" for="da-contact-target"></label>
<form class="da-contact-popin">
    <label for="da-contact-target" class="da-contact-close"></label>
    <h3 class="da-title">Contact Us</h3>
    <input type="text" name="name" placeholder="Name" required>
    <input type="email" name="email" placeholder="Email" required>
    <input type="tel" name="tel" placeholder="Phone" required>
    <textarea name="message" placeholder="Message" required></textarea>
    <input type="submit">
</form>
<div class="da-layer"></div>

<main class="da-container">
    <div class="da-home-landing">
        <div class="da-home-landing-content">
            <h1 class="da-headline">A worldwide creative Wordpress Agency</h1>
            <p class="da-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ante cursus egestas amet
                massa integer eu elementum. Tincidunt id turpis arcu, purus, semper quis posuere ullamcorper
                accumsan.</p>
        </div>
        <nav class="da-home-landing-menu">
            <ul>
                <li><a href="#team">Team</a></li>
                <li><a href="#services">Services</a></li>
                <li><a href="#projects">Projects</a></li>
                <li><a href="#news">News</a></li>
            </ul>
        </nav>
        <div class="da-small-container">
            <div class="da-home-team" id="team">
                <div class="da-home-team-person">
                    <div class="da-home-team-person-image-wrapper">
                        <img src="/assets/images/person-olivier.svg" alt="Illustration représentant Olivier">
                    </div>
                    <span>Olivier</span>
                </div>
                <div class="da-home-team-person">
                    <div class="da-home-team-person-image-wrapper">
                        <img src="/assets/images/person-denis.svg" alt="Illustration représentant Denis">
                    </div>
                    <span>Denis</span>
                </div>
                <div class="da-home-team-person">
                    <div class="da-home-team-person-image-wrapper">
                        <img src="/assets/images/person-florian.svg" alt="Illustration représentant Florian">
                    </div>
                    <span>Florian</span>
                </div>
                <div class="da-home-team-person">
                    <div class="da-home-team-person-image-wrapper">
                        <img src="/assets/images/person-melanie.svg" alt="Illustration représentant Mélanie">
                    </div>
                    <span>Mélanie</span>
                </div>
                <div class="da-home-team-person">
                    <div class="da-home-team-person-image-wrapper">
                        <img src="/assets/images/person-mathilde.svg" alt="Illustration représentant Mathilde">
                    </div>
                    <span>Mathilde</span>
                </div>
            </div>
        </div>
    </div>

    <div class="da-home-services" id="services">
        <h2 class="da-title">What a team for qualitative services</h2>
        <div class="da-small-container">
            <div class="da-home-services-item">
                <div class="da-home-services-item-image-wrapper">
                    <img src="/assets/images/strategy-icon.svg" alt="Icon de strategie">
                </div>
                <h3 class="da-subtitle">Strategy</h3>
                <p class="da-text">Project management, planning, reporting</p>
            </div>
            <div class="da-home-services-item">
                <div class="da-home-services-item-image-wrapper">
                    <img src="/assets/images/innovative-icon.svg" alt="Icon d'inovation">
                </div>
                <h3 class="da-subtitle">Innovative</h3>
                <p class="da-text">Force of proposal, listening to your wildest ideas</p>
            </div>
            <div class="da-home-services-item">
                <div class="da-home-services-item-image-wrapper">
                    <img src="/assets/images/creativity-icon.svg" alt="Icon de créativité">
                </div>
                <h3 class="da-subtitle">Creative</h3>
                <p class="da-text">UX / UI Design</p>
            </div>
            <div class="da-home-services-item">
                <div class="da-home-services-item-image-wrapper">
                    <img src="/assets/images/developpement-icon.svg" alt="Icon de developpement">
                </div>
                <h3 class="da-subtitle">Developpement</h3>
                <p class="da-text">Integration, Optimisation, Developpements</p>
            </div>
        </div>
    </div>
    <div class="da-home-creations" id="projects">
        <h2 class="da-subtitle">Our latest projects</h2>
        <div class="da-small-container da-home-creations-grid">
            <div class="da-home-creations-grid-item">
                <img src="/assets/images/project-1.png" alt="Image du projet 1">
            </div>
            <div class="da-home-creations-grid-item">
                <div class="da-home-creations-grid-item-layer">
                    <div class="da-home-creations-grid-item-layer-wrapper-logo">
                        <img src="/assets/images/logo-illy.svg" alt="Logo de la marque Illy">
                    </div>
                    <p class="da-text">UX/UI Design, Developpement</p>
                </div>
                <img src="/assets/images/project-2.png" alt="Image du projet 2">
            </div>
            <div class="da-home-creations-grid-item">
                <img src="/assets/images/project-3.png" alt="Image du projet 3">
            </div>
            <div class="da-home-creations-grid-item">
                <img src="/assets/images/project-4.png" alt="Image du projet 4">
            </div>
            <div class="da-home-creations-grid-item">
                <img src="/assets/images/project-5.png" alt="Image du projet 5">
            </div>
            <div class="da-home-creations-grid-item">
                <img src="/assets/images/project-6.png" alt="Image du projet 6">
            </div>
        </div>
    </div>
    <div class="da-home-news" id="news">
        <h2 class="da-title">Our News</h2>
        <div class="da-home-news-grid">
            <div class="da-home-news-grid-item">
                <img src="/assets/images/project-2.png" alt="Image du projet 2">
                <h3 class="da-small-title">Les plugins du mois de septembre</h3>
                <p class="da-text">Posté par Olivier <small>il y a 3h</small></p>
            </div>
            <div class="da-home-news-grid-item">
                <img src="/assets/images/project-2.png" alt="Image du projet 2">
                <h3 class="da-small-title">Les plugins du mois de septembre</h3>
                <p class="da-text">Posté par Olivier <small>il y a 3h</small></p>
            </div>
            <div class="da-home-news-grid-item">
                <img src="/assets/images/project-2.png" alt="Image du projet 2">
                <h3 class="da-small-title">Les plugins du mois de septembre</h3>
                <p class="da-text">Posté par Olivier <small>il y a 3h</small></p>
            </div>
        </div>
    </div>
</main>
<footer>
    <div class="da-container">
        <div class="da-footer-copyright-wrapper">
            <a href="#start" class="da-logo-wrapper">
                <img src="/assets/images/logo.svg" alt="Logo de l'agence web DumsAgency">
            </a>
            <p class="da-text">Denis Dumont © <?= date('Y') ?> <br> Tout droits réservés</p>
        </div>
        <nav class="da-footer-menu">
            <ul>
                <li><a href="#team">Team</a></li>
                <li><a href="#services">Services</a></li>
                <li><a href="#projects">Projects</a></li>
                <li><a href="#news">News</a></li>
            </ul>
        </nav>
        <div></div>
    </div>
</footer>
</body>
</html>